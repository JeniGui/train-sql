-- 1、查询"01"课程比"02"课程成绩高的学生的信息及课程分数
-- 思路: "01"的表1, "02"的表2, 然后表1与表2的并表查询  
SELECT stu1.*,cou1.c_id,sco1.s_score,cou2.c_id,sco2.s_score FROM student stu1,score sco1,course cou1,student stu2,score sco2,course cou2
WHERE stu1.s_id=sco1.s_id 
AND sco1.c_id=cou1.c_id

AND stu2.s_id=sco2.s_id 
AND sco2.c_id=cou2.c_id

AND cou1.c_id='01' 
AND cou2.c_id='02'
AND sco1.s_score>sco2.s_score


-- 2、查询"01"课程比"02"课程成绩低的学生的信息及课程分数
-- 思路:参考题1
SELECT stu1.*,cou1.c_id,sco1.s_score,cou2.c_id,sco2.s_score FROM student stu1,score sco1,course cou1,student stu2,score sco2,course cou2
WHERE stu1.s_id=sco1.s_id 
AND sco1.c_id=cou1.c_id

AND stu2.s_id=sco2.s_id 
AND sco2.c_id=cou2.c_id

AND cou1.c_id='01'
AND cou2.c_id='02'
AND sco1.s_score<sco2.s_score
-- 3、查询平均成绩大于等于60分的同学的学生编号和学生姓名和平均成绩
-- 思路: 1.并表查询每个同学的平均成绩(每个人的avg需要分组,by stu.s_id) 2.再条件筛选 3. 没有where+ 聚合函数的写法
-- 并表
SELECT stu.s_id,stu.s_name,AVG(sco.s_score) AS 'avg_score' FROM student stu,score scoWHERE stu.s_id=sco.s_id
GROUP BY stu.s_id
HAVING AVG( sco.s_score) >= 60

-- join
SELECT stu.s_id,stu.s_name,AVG(sco.s_score) AS 'avg_score' FROM student stu
INNER JOIN score sco
ON stu.s_id=sco.s_id
GROUP BY stu.s_id
HAVING AVG( sco.s_score) >= 60


-- 4、查询平均成绩小于60分的同学的学生编号和学生姓名和平均成绩(包括有成绩的和无成绩的)
-- 思路: 1. 查询平均成绩小于60的信息 union  2. 查询没有成绩的学生信息
-- 平均成绩小于60
SELECT stu.s_id,stu.s_name,AVG(sco.s_score) FROM student stu
LEFT JOIN score sco
ON stu.s_id=sco.s_id
GROUP BY stu.s_id,stu.s_name
HAVING AVG( sco.s_score) < 60

UNION

-- 没有成绩的学生信息(成绩表里没有学生表的s_id)
SELECT stu2.s_id,stu2.s_name,0 AS 'avg_score' FROM student stu2
WHERE stu2.s_id NOT IN (SELECT DISTINCT s_id FROM score )

-- 法二
SELECT stu.s_id,stu.s_name,AVG(sco.s_score) FROM student stu
LEFT JOIN score sco
ON stu.s_id=sco.s_id
GROUP BY stu.s_id,stu.s_name
HAVING AVG( sco.s_score) < 60 OR AVG( sco.s_score) IS NULL

-- 5、查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩
-- 思路: 查询每个学生的信息和选课总数,所有课程的总成绩,必然要以学生分组
-- 并表
SELECT stu.s_id,stu.s_name,COUNT(sco.c_id) AS '选课总数',SUM(sco.s_score) '所有课程的总成绩'FROM student stu,score sco
WHERE stu.s_id=sco.s_id
GROUP BY stu.s_id ,stu.s_name

-- join
SELECT stu.s_id,stu.s_name,COUNT(sco.c_id) AS '选课总数',SUM(sco.s_score) '所有课程的总成绩'
FROM student stu
LEFT JOIN score sco
ON stu.s_id=sco.s_id
GROUP BY stu.s_id ,stu.s_name
-- 6、查询"李"姓老师的数量 
SELECT COUNT(t_id) FROM teacher WHERE t_name LIKE '李%';
SELECT COUNT(t_id) FROM teacher WHERE t_name LIKE CONCAT('李','%');
-- 7、查询学过"张三"老师授课的同学的信息 
-- 张三老师授课的课程号02
SELECT cou.c_id FROM course cou,teacher t
WHERE cou.t_id = t.t_id
AND t.t_name='张三'

-- 1.并表
SELECT  stu.* FROM student stu,score sco
WHERE stu.s_id =sco.s_id
AND sco.c_id = (SELECT cou.c_id FROM course cou,teacher t
WHERE cou.t_id = t.t_id
AND t.t_name='张三')

-- 2.join

SELECT stu.* FROM student stu
JOIN score sco 
ON stu.s_id = sco.s_id
WHERE sco.c_id IN(

SELECT cou.c_id FROM course cou WHERE cou.t_id= (SELECT t_id FROM teacher WHERE t_name='张三')
)


-- 8、查询没学过"张三"老师授课的同学的信息 

SELECT stu.* FROM student stu 
WHERE stu.s_id NOT IN (SELECT  stu.s_id FROM student stu,score sco
WHERE stu.s_id =sco.s_id
AND sco.c_id = (SELECT cou.c_id FROM course cou,teacher t
WHERE cou.t_id = t.t_id
AND t.t_name='张三'))




SELECT  stu.*,sco.c_id FROM student stu,score sco
WHERE stu.s_id =sco.s_id

AND sco.c_id NOT IN (SELECT cou.c_id FROM course cou,teacher t
WHERE cou.t_id = t.t_id
AND t.t_name='张三')




-- 9、查询学过编号为"01"并且也学过编号为"02"的课程的同学的信息
-- 思路: 1.学生表stu  2.有'01'课程的成绩表sco1  3.有'02'课程的成绩表sco2  三表并表查询
SELECT  stu.* FROM student stu ,score sco1,score sco2
WHERE stu.s_id = sco1.s_id
AND stu.s_id = sco2.s_id
AND sco1.c_id ='01'
AND sco2.c_id ='02'

-- 10、查询学过编号为"01"但是没有学过编号为"02"的课程的同学的信息
-- 思路: 1.先查询学习过'02'的课程的学生s_id 2.再筛选学过'01'但学号不在s_id里面
SELECT stu.* FROM student stu ,score sco
WHERE stu.s_id = sco.s_id
AND sco.c_id ='01'
AND stu.s_id NOT IN (SELECT s.s_id FROM student s,score sc WHERE s.s_id=sc.s_id AND sc.c_id ='02')
-- 11、查询没有学全所有课程的同学的信息
-- 1.统计课程数
SELECT COUNT(c_id) FROM course;
-- 2.统计所有学生学习课程数量
-- 并表,不建议
SELECT stu.s_id,COUNT(sco.c_id) FROM student stu, score sco
WHERE stu.s_id = sco.s_id
GROUP BY stu.s_id 
-- join
SELECT stu.s_id,COUNT(sco.c_id) FROM student stu
LEFT JOIN score scoON stu.s_id = sco.s_id
GROUP BY stu.s_id 

-- 3.查询没有学满课程数的学生
-- 并表,不建议
SELECT stu.* FROM student stu, score sco
WHERE stu.s_id = sco.s_id
GROUP BY stu.s_id 
HAVING COUNT(sco.c_id) != (SELECT COUNT(c_id) FROM course)


-- join
SELECT stu.* FROM student stu
LEFT JOIN score sco
ON stu.s_id = sco.s_id
GROUP BY stu.s_id 
HAVING COUNT(sco.c_id) != (SELECT COUNT(c_id) FROM course)

-- 12、查询至少有一门课与学号为"01"的同学所学相同的同学的信息 
-- 1.查询学号01同学所学习的课程
SELECT sco.c_id AS '学习课程'FROM score sco,student stu
WHERE sco.s_id = stu.s_idAND stu.s_id='01'
-- 2.
SELECT DISTINCT stu.* FROM student stu,score sco 
WHERE stu.s_id = sco.s_id
AND stu.s_id !='01'
AND sco.c_id IN (SELECT sco.c_id AS '学习课程'FROM score sco,student stu
WHERE sco.s_id = stu.s_id
AND stu.s_id='01')

-- 13、查询和"01"号的同学学习的课程完全相同的其他同学的信息
-- 1.查询学号01同学所学习的课程
SELECT sco.c_id AS '学习课程'FROM score sco,student stu
WHERE sco.s_id = stu.s_id
AND stu.s_id='01'

--
SELECT stu.* FROM student stu
LEFT JOIN score sco
ON stu.s_id = sco.s_id
AND sco.s_id IN(
SELECT sco.c_id AS '学习课程'FROM score sco,student stu
WHERE sco.s_id = stu.s_id
AND stu.s_id='01')
GROUP BY sco.s_id
HAVING stu.s_id != '01'
-- 14、查询没学过"张三"老师讲授的任一门课程的学生姓名 
-- 1.张三老师授课的课程号
SELECT cou.c_id FROM course cou,teacher t
WHERE cou.t_id = t.t_id
AND t.t_name='张三'

-- 2.学过张三老师的课程学生
SELECT   sco.s_id FROM student stu
LEFT JOIN score sco
ON stu.s_id != sco.s_id
AND sco.c_id = (
SELECT cou.c_id FROM course cou,teacher t
WHERE cou.t_id = t.t_id
AND t.t_name='张三')
GROUP BY sco.s_id

-- 
SELECT * FROM studentWHERE s_id NOT IN(
SELECT   sco.s_id FROM student stu
LEFT JOIN score sco
ON stu.s_id != sco.s_id
AND sco.c_id = (
SELECT cou.c_id FROM course cou,teacher t
WHERE cou.t_id = t.t_id
AND t.t_name='张三')
GROUP BY sco.s_id
)

-- 15、查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩(及格为>=60) 
-- 1. 2门及以上不及格课程的学生 

-- 2. 
SELECT stu.s_id,stu.s_name,AVG(sco.s_score) FROM student stu
LEFT JOIN score sco 
ON stu.s_id=sco.s_id
AND sco.s_score <60
GROUP BY sco.s_id
HAVING COUNT(sco.s_score < 60) >=2

UNION

SELECT stu.s_id,stu.s_name,AVG(sco.s_score) FROM student stu
LEFT JOIN score sco 
ON stu.s_id=sco.s_id
GROUP BY sco.s_id
HAVING AVG(sco.s_score) IS NULL


-- 16、检索"01"课程分数小于60，按分数降序排列的学生信息
SELECT stu.* ,sco.s_score FROM student stu
LEFT JOIN score sco 
ON stu.s_id = sco.s_id
AND sco.c_id='01'
AND sco.s_score < 60
ORDER BY sco.s_score DESC

-- 17、按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
-- 按平均成绩从高到低显示所有学生的所有课程的成绩
SELECT s_id, c_id,s_score FROM score 
GROUP BY s_id,c_id
ORDER BY AVG(s_score) DESC

-- 解法1
SELECT  a.s_id,(SELECT s_score FROM score WHERE s_id =a.s_id AND c_id = '01') AS '语文',

(SELECT s_score FROM score WHERE s_id =a.s_id AND c_id = '02') AS '数学',
(SELECT s_score FROM score WHERE s_id =a.s_id AND c_id = '02') AS '英语',

ROUND(AVG(s_score),2) AS '平均分' 
FROM score a GROUP BY a.s_id ORDER BY 平均分 DESC

-- 解法2



-- 18.查询各科成绩最高分、最低分和平均分：以如下形式显示：课程ID，课程name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
SELECT sco.c_id AS '课程ID', cou.c_name AS '课程name',
MAX(s_score) AS '最高分',MIN(s_score) AS '最低分',ROUND(AVG(s_score),2) AS '平均分' ,

ROUND(100 * (SUM( CASE WHEN sco.s_score >= 60 THEN 1 ELSE 0 END )/SUM(CASE WHEN sco.s_score THEN 1 ELSE 0 END)),2) AS '及格率',
ROUND(100 * (SUM( CASE WHEN sco.s_score >= 60 AND sco.s_score < 70 THEN 1 ELSE 0 END )/SUM(CASE WHEN sco.s_score THEN 1 ELSE 0 END)),2) AS '中等率',
ROUND(100 * (SUM( CASE WHEN sco.s_score >= 80 AND sco.s_score <90 THEN 1 ELSE 0 END )/SUM(CASE WHEN sco.s_score THEN 1 ELSE 0 END)),2) AS '优良率',
ROUND(100 * (SUM( CASE WHEN sco.s_score >= 90 THEN 1 ELSE 0 END )/SUM(CASE WHEN sco.s_score THEN 1 ELSE 0 END)),2) AS '优秀率'

FROM score scoLEFT JOIN course cou
ON sco.c_id=cou.c_id
GROUP BY sco.c_id,cou.c_name



-- 19、按各科成绩进行排序，并显示排名 #
-- mysql没有rank函数


-- 20、查询学生的总成绩并进行排名#

-- 学生的总成绩
SELECT stu.s_id,SUM(sco.s_score) FROM student stu

LEFT JOIN score sco
ON stu.s_id=sco.s_id

GROUP BY stu.s_id 
ORDER BY SUM(sco.s_score) DESC

-- 排名





-- 21、查询不同老师所教不同课程平均分从高到低显示 

SELECT t.t_name,cou.c_name FROM course cou,teacher t
WHERE t.t_id = cou.t_id




SELECT t.t_name,cou.c_name, ROUND(AVG(sco.s_score),2) AS '平均分' FROM score sco,course cou,teacher t
WHERE sco.c_id=cou.c_id
AND t.t_id = cou.t_id
GROUP BY t.t_id,sco.c_id
ORDER BY 平均分 DESC

-- join

SELECT t.t_name,cou.c_name, ROUND(AVG(sco.s_score),2) AS '平均分' FROM score sco
LEFT JOIN course cou
ON sco.c_id=cou.c_id
LEFT JOIN teacher t
ON t.t_id = cou.t_id
GROUP BY t.t_id,sco.c_id
ORDER BY 平均分 DESC

-- 22、查询所有课程的成绩第2名到第3名的学生信息及该课程成绩 #
SELECT sco.s_id,sco.c_id,sco.s_score FROM score sco
WHERE sco.c_id='01'
GROUP BY sco.s_id,sco.c_id
ORDER BY sco.s_score DESC
LIMIT 1,2

SELECT stu.s_id,stu.s_name,
(SELECT s_score FROM score WHERE s_id =sco.s_id AND c_id = '01') AS '语文',(SELECT s_score FROM score WHERE s_id =sco.s_id AND c_id = '02') AS '数学',
(SELECT s_score FROM score WHERE s_id =sco.s_id AND c_id = '03') AS '英语'
FROM student stu, score sco

WHERE stu.s_id=sco.s_id
GROUP BY sco.s_id
ORDER BY sco.s_score DESC
LIMIT 1,2


-- 23、统计各科成绩各分数段人数：课程编号,课程名称,[100-85],[85-70],[70-60],[0-60]及所占百分比

-- 24、查询学生平均成绩及其名次 #
-- 学生平均成绩
SELECT s_id,AVG(s_score) FROM score
GROUP BY s_id
ORDER BY AVG(s_score) DESC
-- 名次

-- 25、查询各科成绩前三名的记录

-- 1.选出b表比a表成绩大的所有组

-- 2.选出比当前id成绩大的 小于三个的

-- 26、查询每门课程被选修的学生数 
SELECT co.c_name AS '选修课', COUNT(sc.s_id) AS '学生数' FROM course co, score sc 
WHERE co.c_id=sc.c_id
GROUP BY sc.c_id ;




-- 27、查询出只有两门课程的全部学生的学号和姓名

-- 第一种
SELECT s.s_id AS '学号',s.s_name AS '姓名' FROM student s 
JOIN score sc ON s.s_id=sc.s_id
JOIN course co ON sc.c_id=co.c_id
GROUP BY sc.s_id HAVING COUNT(sc.c_id)=2
-- 第二种
SELECT s_id,s_name FROM student WHERE s_id IN(

SELECT s_id FROM score GROUP BY s_id HAVING COUNT(c_id)=2);


-- 28、查询男生、女生人数
SELECT s_sex,COUNT(s_sex) FROM student
GROUP BY s_sex

-- 29、查询名字中含有"风"字的学生信息

SELECT * FROM student 
WHERE s_name LIKE '%风%';

-- 30、查询同名同性学生名单，并统计同名人数 
SELECT s_name,s_sex,COUNT(*) AS '同名人数量' FROM student
GROUP BY s_name,s_sex HAVING COUNT(*)>=2;

-- 31、查询1990年出生的学生名单
-- 第一种:year函数，YEAR('2011-11-11 11:11:11') --2011 
SELECT * FROM student WHERE YEAR(s_birth)='1990';

-- 第2种：like
select * from student where s_birth LIKE '1990%';

-- 32、查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列 


-- 33、查询平均成绩大于等于85的所有学生的学号、姓名和平均成绩 
SELECT stu.s_id,stu.s_name, AVG(sco.s_score) FROM student stu,score sco
WHERE stu.s_id= sco.s_id
GROUP BY sco.s_id HAVING AVG(sco.s_score) >= 80

-- 34、查询课程名称为"数学"，且分数低于60的学生姓名和分数 
-- 写法1：并表
SELECT stu.s_name,sco.s_score FROM student stu,course cou,score sco
WHERE stu.s_id=sco.s_id AND sco.c_id=cou.c_id
AND cou.c_name='数学' AND sco.s_score<60

-- 写法2：子查询
SELECT stu.s_name,sco.s_score FROM student stu,score sco
WHERE stu.s_id=sco.s_id 
AND sco.c_id=(SELECT c_id FROM course WHERE c_name='数学')
AND sco.s_score<60

-- 写法3：join
SELECT stu.s_name,sco.s_score FROM student stu
JOIN score sco ON stu.s_id=sco.s_id
JOIN course cou ON sco.c_id=cou.c_id 
AND cou.c_name='数学' AND sco.s_score<60

-- 35、查询所有学生的课程及分数情况；
-- 每个学生的课程和分数，意味这需要对学生分组（s_id或s_name,s_id）,
-- 然后每行显示一个学生的3门课程分数，需要使用条件case..when..else
-- 最后学生课程分数为null的也需要显示，需要用到left join(跟学生有关联的数据，null值也要显示)
SELECT stu.s_id,stu.s_name, 
SUM(CASE cou.c_name WHEN '语文' THEN sco.s_score ELSE 0  END) AS '语文',
SUM(CASE cou.c_name WHEN '数学' THEN sco.s_score ELSE 0  END) AS '数学',
SUM(CASE cou.c_name WHEN '英语' THEN sco.s_score ELSE 0  END) AS '英语',
SUM(sco.s_score) AS '总分'
FROM student stu
LEFT JOIN score sco ON stu.s_id=sco.s_id
LEFT JOIN course cou ON sco.c_id=cou.c_id 
GROUP BY stu.s_id,stu.s_name

-- 36、查询任何一门课程成绩在70分以上的姓名、课程名称和分数； 
-- 写法1：并表
SELECT stu.s_name,cou.c_name,sco.s_score FROM student stu,course cou,score sco
WHERE stu.s_id=sco.s_id AND sco.c_id=cou.c_id
AND sco.s_score>70
-- group by stu.s_id 

-- join
SELECT a.s_name,b.c_name,c.s_score FROM course b LEFT JOIN score c ON b.c_id = c.c_id

LEFT JOIN student a ON a.s_id=c.s_id WHERE c.s_score>=70


-- 37、查询不及格的课程(<60)
SELECT stu.s_id,stu.s_name, cou.c_name,sco.s_score
FROM student stu
LEFT JOIN score sco ON stu.s_id=sco.s_id
LEFT JOIN course cou ON sco.c_id=cou.c_id 
WHERE sco.s_score<60



-- 38、查询课程编号为01且课程成绩在80分以上的学生的学号和姓名；
-- where并表
SELECT stu.s_id,stu.s_name FROM student stu,score sco
WHERE stu.s_id=sco.s_id AND sco.c_id='01' AND sco.s_score>80
-- group by stu.s_id,stu.s_name


--  join连接
SELECT stu.s_id,stu.s_name
FROM student stu
JOIN score sco ON stu.s_id=sco.s_id
WHERE sco.c_id='01' AND sco.s_score>80

-- 39、求每门课程的学生人数 


-- 40、查询选修"张三"老师所授课程的学生中，成绩最高的学生信息及其成绩
-- 查询老师id	
-- 查询最高分（可能有相同分数）
-- 查询信息


-- 41、查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩 

-- 42、查询每门功成绩最好的前两名 
-- 牛逼的写法


-- 43、统计每门课程的学生选修人数（超过5人的课程才统计）。要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列  

-- 44、检索至少选修两门课程的学生学号 
-- 45、查询选修了全部课程的学生信息 


-- 46、查询各学生的年龄

-- 按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一

-- 47、查询本周过生日的学生

-- 48、查询下周过生日的学生

-- 49、查询本月过生日的学生
-- 50、查询下月过生日的学生